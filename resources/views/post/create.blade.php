@extends('adminlte.master')

@section('content')
<div class="card card-primary m-3">
    <div class="card-header">
      <h3 class="card-title">Add New Cast</h3>
    </div>
    <!-- /.card-header -->
    <!-- form start -->
    <form action="/cast" method="POST">
      @csrf
      <div class="card-body">
        <div class="form-group">
          <label for="name">Name :</label>
          <input type="text" class="form-control" name="name" id="name" placeholder="Enter name">
        </div>
        @error('name')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="umur">Umur :</label>
          <input type="text" class="form-control" name="umur" id="umur" placeholder="Enter age">
        </div>
        @error('umur')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
        <div class="form-group">
          <label for="bio">Bio :</label>
          <textarea class="form-control" rows="5" name="bio" id="bio" placeholder="Bio"></textarea>
        </div>
        @error('bio')
            <div class="alert alert-danger">{{ $message }}</div>
        @enderror
      </div>
      <!-- /.card-body -->

      <div class="card-footer">
        <button type="submit" class="btn btn-primary" value="submit">Submit</button>
      </div>
    </form>
  </div>
   
@endsection
