<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class pemeran extends Model
{
    protected $table="cast";
    protected $fillable = ["name","umur","bio"];
}
