<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AuthController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\CastController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'] );
Route::get('/form', [AuthController::class, 'register'] );
Route::post('/register', [AuthController::class, 'welcome'] );
Route::get('master', function(){
    return view('adminlte/master');
} );

Route::get('table', function(){
    return view('items/index');
} );
Route::get('data-tables', function(){
    return view('items/data_tabel');
} );

// Route::get('/cast', [CastController::class, 'index'] );
// Route::get('/cast/create', [CastController::class, 'create'] );
// Route::post('/cast', [CastController::class, 'store'] );
// Route::get('/cast/{cast_id}', [CastController::class, 'show'] );
// Route::get('/cast/{cast_id}/edit', [CastController::class, 'edit'] );
// Route::put('/cast/{cast_id}', [CastController::class, 'update'] );
// Route::delete('/cast/{cast_id}', [CastController::class, 'destroy'] );

Route::resource('cast', CastController::class);